//
//  RestaurantListViewController.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/23/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit

class RestaurantListViewController: UIViewController, loadRestaurantsDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    var network = Networking()
    var myRestaurants: RestaurantsResponse?
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.network.fetchRestaurants()
        }
        

        network.restaurantDelegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    
    func didLoadRestaurants(restaurantsResponse: RestaurantsResponse) {
        
        self.myRestaurants = restaurantsResponse
        tableView.reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "RestaurantsToOrders" {
            
            let desticnationVC = segue.destination as! OrdersViewController
            desticnationVC.restaurant = sender as? Restaurant
            
        }
        
    }

}

//MARK: - TabelVeiw Delegate Methods

extension RestaurantListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.myRestaurants?.restaurantArray.count ?? 0
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
     
        let rest = (myRestaurants?.restaurantArray[indexPath.row])!
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell") as! RestaurantListTableViewCell
        
        cell.setRestaurant(restaurant: rest)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let safeMyRestaurants = myRestaurants else { return }
        
        let rest = (safeMyRestaurants.restaurantArray[indexPath.row])
        
        performSegue(withIdentifier: "RestaurantsToOrders", sender: rest)
        
        
    }
    
    
    
}

//
//  LoginViewController.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/22/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, checkLoginDelegate {
    
    @IBOutlet weak var numberTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    var network = Networking()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        network.userDelegate = self
        
    }
    
    
    
    @IBAction func loginPressed(_ sender: UIButton) {
        
        let userNumber = numberTextfield.text
        let userPassword = passwordTextfield.text
        
        if (userNumber?.isEmpty)! || (userPassword?.isEmpty)! {
            
            displayMessage(userMessage: "One of the required fields is missing")
            return
            
        }
        
        DispatchQueue.main.async {
            self.network.checkLogin(userNumber: userNumber!, userPassword: userPassword!)
            
        }
        
    }
    //MARK: - Network functions
    
    func didCheckUser(user: User) {
        
        
        if user.message == "logged In" {
            performSegue(withIdentifier: "LoginToRestaurantList", sender: self)
        } else {
            displayMessage(userMessage: user.message)
        }
        
    }
    
    //MARK: - UI Fucntions
    
    
    func displayMessage(userMessage: String) {
        
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action: UIAlertAction) in
                
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    

}


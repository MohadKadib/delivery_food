//
//  RestaurantListTableViewCell.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/23/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit
import SDWebImage


class RestaurantListTableViewCell: UITableViewCell {

    @IBOutlet weak var restaurantImageView: UIImageView!
    
    
    @IBOutlet weak var restaurantNameLabel: UILabel!
    
    @IBOutlet weak var restaurantTypeLabel: UILabel!
    
    @IBOutlet weak var locatoinMapLabel: UILabel!
    
    
    func setRestaurant(restaurant: Restaurant) {
        restaurantImageView.sd_setImage(with: URL(string: restaurant.restImageSource), placeholderImage: UIImage(named: "restaurant logo.png"))
        restaurantNameLabel.text = restaurant.restName
        restaurantTypeLabel.text = restaurant.restType
        locatoinMapLabel.text = restaurant.restLocation
    }
    
    
}

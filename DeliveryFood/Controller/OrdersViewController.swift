//
//  OrdersViewController.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/24/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class OrdersViewController: UIViewController, loadOrdersDelegate {
    
    
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    var restaurant: Restaurant?
    var myOrders: OrdersResponse?
    var network = Networking()
    let restaurantAnnotation = MKPointAnnotation()
    let locationManager = CLLocationManager()
    var ordersAnnotations = [MKPointAnnotation]()
    var currentRoute: MKRoute?
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        DispatchQueue.main.async {

            self.network.fetchOrders(restID: self.restaurant!.restID)
        }
        
        network.orderDelegate = self
        centerViewOnUserLocation()
        
    }
    
    func didLoadOrders(ordersResponse: OrdersResponse) {
        
        self.myOrders = ordersResponse
        addOrdersAnnotations()
        constructRoute(restaurantLocation: restaurantAnnotation.coordinate)
        
    }
    
    
    
    
    //MARK: - Annotations Functions
    func centerViewOnUserLocation() {
        
        guard let safeRestaurant = restaurant else{ return }
        
        let pLat = (safeRestaurant.restLocationLatitude)
        let pLong = (safeRestaurant.restLocationLongitude)
        let center = CLLocationCoordinate2D(latitude: pLat, longitude: pLong)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 5, longitudeDelta: 5))
        mapView.setRegion(region, animated: true)
        restaurantAnnotation.coordinate = center
        mapView.addAnnotation(restaurantAnnotation)
        
    }
    
    
    func addOrdersAnnotations() {
        
        if let orderArray = myOrders?.orderArray {
            
            for order in orderArray {
                
                let pLat = order.orderLocationLatitude
                let pLong = order.orderLocationLongitude
                let annotation = MKPointAnnotation()
                annotation.title = order.orderUser
                annotation.coordinate = CLLocationCoordinate2D(latitude: pLat, longitude: pLong)
                mapView.addAnnotation(annotation)
                ordersAnnotations.append(annotation)
                
            }
            
        } else {
            print("orders array is nil")
        }
        
    }
    
    
    func constructRoute(restaurantLocation: CLLocationCoordinate2D) {
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = MKMapItem(placemark: MKPlacemark(coordinate: restaurantLocation))
        directionRequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: ordersAnnotations[0].coordinate))
        directionRequest.requestsAlternateRoutes = true
        directionRequest.transportType = .any
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate { [weak self] (directionsResponse, error) in
            
            guard let strongSelf = self else { return }
            
            if let error = error {
                
                print(error.localizedDescription)
                
            } else if let response = directionsResponse, response.routes.count > 0 {
                
                strongSelf.currentRoute = response.routes[0]
                strongSelf.mapView.addOverlay(response.routes[0].polyline)
                strongSelf.mapView.setVisibleMapRect(response.routes[0].polyline.boundingMapRect, animated: true)
                
            }
            
        }
        
    }
    
    

}

extension OrdersViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        guard let currentRoute = currentRoute else {
            return MKOverlayRenderer()
        }
        
        let polyLineRenderer = MKPolylineRenderer(polyline: currentRoute.polyline)
        polyLineRenderer.strokeColor = UIColor.red
        polyLineRenderer.lineWidth = 5
        
        return polyLineRenderer
        
    }
    
}





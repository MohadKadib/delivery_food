//
//  Restaurants.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/27/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import Foundation

struct RestaurantsResponse {
    
    let status: Int
    let sub_message: String
    let message: String
    let restaurantArray: [Restaurant]
    
    
}

struct Restaurant {
    
    let restID: String
    let restName: String
    let restImageSource: String
    let restLocation: String
    let restType: String
    var restLocationLatitude: Double {
        return Restaurant.convertStringLocationToDouble(locationString: restLocation, longOrLat: 0)
    }
    var restLocationLongitude: Double {
        return Restaurant.convertStringLocationToDouble(locationString: restLocation, longOrLat: 1)
    }
    
    
    
    
    static func convertStringLocationToDouble(locationString: String, longOrLat: Int) -> Double {
        
        let temp = locationString.replacingOccurrences(of: " ", with: "")
        let temp1 = temp.components(separatedBy: ",")
        
        return Double(temp1[longOrLat])!
        
    }
    
}

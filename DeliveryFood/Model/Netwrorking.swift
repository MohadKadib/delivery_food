//
//  Netwrorking.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/25/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

protocol checkLoginDelegate {
    func didCheckUser(user: User)
}

protocol loadRestaurantsDelegate {
    func didLoadRestaurants(restaurantsResponse: RestaurantsResponse)
}

protocol loadOrdersDelegate {
    func didLoadOrders(ordersResponse: OrdersResponse)
}

struct Networking {
    

    
    var userDelegate: checkLoginDelegate?
    var restaurantDelegate: loadRestaurantsDelegate?
    var orderDelegate: loadOrdersDelegate?
    
    //MARK: - Login Networking Function
    
    func checkLogin(userNumber: String, userPassword: String) {
        
        let url = "http://talabat.art4muslim.net/api/login"
    
        let parameters: [String : String] = [
            "mobile": userNumber,
            "password": userPassword,
            "access_key": "Gdka52DASWE3DSasWE742Wq",
            "access_password": "yH52dDDF85sddEWqPNV7D12sW5e"
        ]
        
        
        AF.request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default).responseJSON { response in
            
            if let loginData = response.value {
                
                let loginDataJSON : JSON = JSON(loginData)
                let user = User(age: loginDataJSON["return"]["age"].intValue,
                                      gneder: loginDataJSON["return"]["gender"].stringValue,
                                      id: loginDataJSON["return"]["id"].intValue,
                                      mobile: loginDataJSON["return"]["mobile"].intValue,
                                      message: loginDataJSON["message"].stringValue,
                                      sub_message: loginDataJSON["sub_message"].stringValue,
                                      status: loginDataJSON["status"].intValue)
                
                self.userDelegate?.didCheckUser(user: user)
                
            } else {
                
                print("error in API data")
                
            }
            
        }
        
    }
    
    
    //MARK: - Getting Restaurants Networking Function
    
    func fetchRestaurants () {
        
        let url = "http://talabat.art4muslim.net/api/getResturants?langu=ar"
        
        AF.request(url,method: .get).responseJSON { response in
            
            if let restaurantsData = response.value {
                
                let restaurantsDataJSON: JSON = JSON(restaurantsData)
                
                var restaurantsArray = [Restaurant]()
                
                for rest in restaurantsDataJSON["return"].arrayValue {
                    
                    
                    let tempRest = Restaurant(restID: rest["rest_id"].stringValue,
                                              restName: rest["rest_name"].stringValue,
                                              restImageSource: rest["rest_img"].stringValue,
                                              restLocation: rest["rest_location"].stringValue,
                                              restType: rest["rest_type"].stringValue)
                    restaurantsArray.append(tempRest)
                    
                }
                
                let restaurantsResponse = RestaurantsResponse(status: restaurantsDataJSON["status"].intValue,
                                              sub_message: restaurantsDataJSON["sub_message"].stringValue,
                                              message: restaurantsDataJSON["message"].stringValue,
                                              restaurantArray: restaurantsArray)
                
                self.restaurantDelegate?.didLoadRestaurants(restaurantsResponse: restaurantsResponse)
                
            } else {
                
                print("error in API data")
                
            }
            
        }
        
    }
    
    //MARK: - Getting Orders Networking Function
    
    
    func fetchOrders(restID: String) {
        
        let url = "http://talabat.art4muslim.net/api/getOrder?restId=\(restID)&langu=ar"
        
        AF.request(url, method: .get).responseJSON { response in
            
            if let ordersData = response.value {
                
                let ordersDataJSON: JSON = JSON(ordersData)
                
                var ordersArray = [Order]()
                
                for order in ordersDataJSON["return"].arrayValue {
                    
                    var productsArray = [Product]()
                    
                    for product in order["order_details"].arrayValue {
                        
                        let tempProduct = Product(prodName: product["prod_name"].stringValue,
                                                  prodQuantity: product["prod_quantity"].intValue,
                                                  prodPrice: product["prod_price"].stringValue,
                                                  prodImage: product["prod_image"].stringValue)
                        productsArray.append(tempProduct)
                        
                    }
                    
                    
                    let tempOrder = Order(orderID: order["order_id"].stringValue,
                                          orderUser: order["order_user"].stringValue,
                                          orderPrice: order["order_price"].stringValue,
                                          userLocation: order["user_location"].stringValue,
                                          restaurantName: order["resturant_name"].stringValue,
                                          orderDetails: productsArray)
                    ordersArray.append(tempOrder)
                    
                }
                
                let ordersResponse = OrdersResponse(status: ordersDataJSON["status"].intValue,
                                                    sub_message: ordersDataJSON["sub_message"].stringValue,
                                                    message: ordersDataJSON["message"].stringValue,
                                                    orderArray: ordersArray)
                
                self.orderDelegate?.didLoadOrders(ordersResponse: ordersResponse)
                
            } else {
                
                print("error in API data")
                
            }
            
        }
        
    }
    
    
    
    
    
}

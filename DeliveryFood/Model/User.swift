//
//  User.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/27/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import Foundation

struct User {
    
    let age: Int
    let gneder: String
    let id: Int
    let mobile: Int
    let message: String
    let sub_message: String
    let status: Int
    
}

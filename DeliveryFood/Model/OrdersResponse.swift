//
//  OrdersResponse.swift
//  DeliveryFood
//
//  Created by mohammed abdulla kadib on 3/28/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import Foundation

struct OrdersResponse {
    
    let status: Int
    let sub_message: String
    let message: String
    let orderArray: [Order]
    
}

struct Order {
    
    let orderID: String
    let orderUser: String
    let orderPrice: String
    let userLocation: String
    let restaurantName: String
    let orderDetails: [Product]
    
    var orderLocationLatitude: Double {
        return Restaurant.convertStringLocationToDouble(locationString: userLocation, longOrLat: 0)
    }
    
    var orderLocationLongitude: Double {
        return Restaurant.convertStringLocationToDouble(locationString: userLocation, longOrLat: 1)
    }
    
    
}

struct Product {
    
    let prodName: String
    let prodQuantity: Int
    let prodPrice: String
    let prodImage: String
    
}
